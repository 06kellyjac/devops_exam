# setting the shell to noninteractive
export DEBIAN_FRONTEND='noninteractive'
# Setting up debconf to automate the mysql-server installation
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password root"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password root"
# Installs git, expect and mysql-server
echo 'Installing: git, expect, mysql-server'
echo '...'
sudo apt-get install -yqq git expect mysql-server

# Checks the mysql service is running
echo ' '
echo 'mysql status:'
sudo service mysql status
echo ' '

# Setting up mysql_secure_installation to be completed automatically
SECURE_MYSQL=$(expect -c '
set timeout 5
spawn mysql_secure_installation
expect "Enter current password for root (enter for none):"
send "root\r"
expect "Change the root password?"
send "n\r"
expect "Remove anonymous users?"
send "y\r"
expect "Disallow root login remotely?"
send "y\r"
expect "Remove test database and access to it?"
send "y\r"
expect "Reload privilege tables now?"
send "y\r"
expect eof
')
echo 'Running automated mysql_secure_installation:'
echo "$SECURE_MYSQL"
echo ' '

# Clones the northwind git repo
git clone 'https://gitlab.cs.cf.ac.uk/CM6212/northwind.git'
echo ' '
# Adds up the northwind schema on the database
echo 'Adding northwind schema and data:'
mysql --user=root --password=root < ./northwind/northwind.sql
# Adds the northwind data to the database
mysql --user=root --password=root < ./northwind/northwind-data.sql

git clone 'https://github.com/06kellyjac/devops-exam-real.git'
echo ' '
# Runs the script on the database and outputs to the log file
echo 'Running auto.sql and outputting to log.txt'
mysql --user=root --password=root < ./devops-exam-real/test.sql > result.orig