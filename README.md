# Repository for my 2nd year DevOps Exam @ Cardiff University

This repository doesn't serve much of a purpose.

## Summary

This script allows for the automatic setup of a virtual machine.

## How to run the script

### Fully Automated using Vagrant

<!-- markdownlint-disable MD006 MD029 -->

1. Copy the contents of `auto.sh` from this repo
2. In your `Vagrantfile` uncomment the area from `config.vm.provision "shell", inline: <<-SHELL` to the next occurrence of `SHELL`
  * This is likely at the end of the file
  * To uncomment remove the `#` symbols
3. Empty the area between the 2 `SHELL` tags
4. Paste the contents of `auto.sh` between the 2 `SHELL` tags
5. Remember to Save the `Vagrantfile`
6. Run `Vagrant`
7. **Done**

<!-- markdownlint-enable MD006 MD029 -->

### Semi Automated

<!-- markdownlint-disable MD006 MD029 -->

1. Start your Ubuntu VM
2. Install `git` if necessary
  * `sudo apt-get install -y git` should work
3. Run `git clone https://github.com/06kellyjac/devops-exam-real.git`
4. Run `bash ./devops-exam-real/auto.sh`
5. **Done**

<!-- markdownlint-enable MD006 MD029 -->

## Details

### `apt-get install`

Packages installed:

* `Git`
* `Expect`
* `MySQL-Server`

### Automated steps

* Sets up `debconf` to automate the install of `mysql-server`
* runs `apt-get install` to install necessary packages
* Sets up `expect` to automate the running of `mysql_secure_installation`
* Imports Northwind and sets up the Schema and Data
* Runs SQL from a repository and writes the output to a `log file`
* Compares the `log file` to a `previous log file` to check repeatability

## Requirements and Potential Issues/Errors

### Requirements

* Vagrant
* VirtualBox
* Internet access for the following:
  * http://vagrantup.com
    * for pulling the vagrant box from Hashicorp
  * Ubuntu repositories
    * for `apt-get install` to pull from
  * Git repositories:
    * https://gitlab.cs.cf.ac.uk/CM6212/northwind.git
      * for Northwind Schema and Data
    * This repo
      * for the SQL that is ran on the Northwind database and the comparison log file
      * Links
        * Web: https://gitlab.com/06kellyjac/devops_exam
        * `Git`:
          * `SSH`: git@gitlab.com:06kellyjac/devops_exam.git
          * `HTTPS`: https://gitlab.com/06kellyjac/devops_exam.git

#### Issues/Potential Errors

* If the VirtualBox GUI is open when vagrant is running there is a rare chance that the install will fail
* If there is already an instance of the Vagrant VM when you run Vagrant there can be issues
* If any of the requirements aren't met then there can be problems such as there not being SQL to run or no access to the required packages for the auto installation
* In the event the `expect` statement fails there will be no issue with the running of `auto.sh` however you should run `mysql_secure_installation` yourself later
