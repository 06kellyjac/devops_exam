use northwind;
show tables;
select count(*) from customers;

-- Additional tests --

-- 10 Additional count() tests to check some  --
-- of the other 20 tables have been populated --

SELECT count(*) AS 'employees Count' FROM employees;
SELECT count(*) AS 'employee_privileges Count' FROM employee_privileges;
SELECT count(*) AS 'invoices Count' FROM invoices;
SELECT count(*) AS 'orders Count' from orders;
SELECT count(*) AS 'orders_status Count' FROM orders_status;
SELECT count(*) AS 'order_details Count' FROM order_details;
SELECT count(*) AS 'order_details_status Count' FROM order_details_status;
SELECT count(*) AS 'products Count' FROM products;
SELECT count(*) AS 'shippers Count' FROM shippers;
SELECT count(*) AS 'suppliers Count' FROM suppliers;

--    5 Additional tests to check    --
-- referential integrity e.g. JOINs  --

-- Join Orders to Customers --
SELECT
	customers.id,
	customers.first_name,
	customers.last_name,
	customers.job_title,
	orders.order_date
FROM customers
LEFT JOIN orders
ON customers.id = orders.customer_id;

-- Join Orders to Shippers --
SELECT
	shippers.id,
	shippers.company,
	orders.shipping_fee,
	orders.ship_address,
	orders.ship_city,
	orders.ship_state_province,
	orders.ship_country_region,
	orders.ship_zip_postal_code
FROM shippers
LEFT JOIN orders
ON shippers.id = orders.shipper_id;

-- Join Order Details & Products to Orders --
SELECT
	orders.id,
	products.product_name,
	order_details.unit_price,
	order_details.quantity,
	orders.order_date
FROM orders
LEFT JOIN order_details
ON orders.id = order_details.order_id
LEFT JOIN products
ON order_details.product_id = products.id;

-- Join Purchase Order, Purchase Order Details & Products to Employees
SELECT
	employees.id,
	employees.first_name,
	employees.last_name,
	purchase_orders.creation_date,
	purchase_orders.payment_amount,
	products.product_name
FROM employees
LEFT JOIN purchase_orders
ON employees.id = purchase_orders.created_by
LEFT JOIN purchase_order_details
ON purchase_orders.id = purchase_order_details.purchase_order_id
LEFT JOIN products
ON purchase_order_details.product_id = products.id;

-- Join Employee Priviledges & Priviledges to Employees --
SELECT
	employees.id,
	employees.first_name,
	employees.last_name,
	privileges.privilege_name
FROM employees
LEFT JOIN employee_privileges
ON employees.id = employee_privileges.employee_id
LEFT JOIN privileges
ON employee_privileges.employee_id = privileges.id;