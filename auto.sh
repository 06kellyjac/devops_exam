# setting the shell to noninteractive
export DEBIAN_FRONTEND='noninteractive'
# Setting up debconf to automate the mysql-server installation
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password root"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password root"
# Installs git, expect and mysql-server using apt-get
echo 'Installing: git, expect, mysql-server'
echo 'This may take a while depending on your internet connection, please be patient ...'
sudo apt-get install -y git expect mysql-server
echo 'Install done'

# Checks the mysql service is running
echo ' '
echo 'MySQL status:'
sudo service mysql status
echo ' '

# Setting up mysql_secure_installation to be completed automatically
# using Expect
SECURE_MYSQL=$(expect -c '
set timeout 5
spawn mysql_secure_installation
expect "Enter current password for root (enter for none):"
send "root\r"
expect "Change the root password?"
send "n\r"
expect "Remove anonymous users?"
send "y\r"
expect "Disallow root login remotely?"
send "y\r"
expect "Remove test database and access to it?"
send "y\r"
expect "Reload privilege tables now?"
send "y\r"
expect eof
')
echo 'Running automated mysql_secure_installation:'
echo "$SECURE_MYSQL"
echo ' '

# Clones the northwind git repo for adding the schema and data to the database
git clone 'https://gitlab.cs.cf.ac.uk/CM6212/northwind.git'
echo ' '

# Adds up the northwind schema on the database
echo 'Adding northwind schema and data'
mysql --user=root --password=root < ./northwind/northwind.sql

# Adds the northwind data to the database
mysql --user=root --password=root < ./northwind/northwind-data.sql

git clone 'https://github.com/06kellyjac/devops-exam-real.git'
echo ' '

# Runs the SQL script on the database and outputs to the log file as result.new
echo 'Running auto.sql and outputting to log.txt'
mysql --user=root --password=root < ./devops-exam-real/test.sql > result.new
echo ' '

# Compares the result.new with the result.orig from the devops-exam-real git repo
# If the logs are a match it will log out that is matched
if cmp -s "./result.new" "./devops-exam-real/result.orig"
then
	echo "Logs Match"
	# Exit with successful exit code
	exit 0
else
	echo "ERROR: Logs don't match"
	# Exit with failure exit code
	exit 1
fi